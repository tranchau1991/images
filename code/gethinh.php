<?php

//$file='code/source/a.png';
$folder = 'code/source/';
$file= $folder.rand(1, 10).".jpg";
//echo getcwd() ; ///Users/CHAU/Documents/htdoc/test/router

//https://github.com/eventviva/php-image-resize
use \Eventviva\ImageResize;

if(file_exists($file)){
    //echo $file;
}

$image = new ImageResize($file);
//$image->scale(50);
//$image->resize(200, 200);

if(isset($w) && isset($h)){
    $image->resize($w, $h);
}

if(isset($s)){
    switch ($s) {
        case '1':
            $image->resize(100, 100);
            break;        
        case '2':
            $image->resize(200, 200);
            break;        
        case '3':
            $image->resize(300, 300);
            break;
        case '4':
            $image->resize(400, 400);
            break;
        case '5':
            $image->resize(500,500);
            break;   
        case '6':
            $image->resize(600,600);
            break;       
        case '7':
            $image->resize(700,700);
            break;       
        case '8':
            $image->resize(800,800);
            break;       
        case '9':
            $image->resize(900,900);
            break;       
        default:
            $image->resize(300,300);
            # code...
            break;
    }
}

//$image->crop(200, 200);
//$image->save('image2.jpg');
echo $image->output();

die;


//resizeImage($file,'200','200',1,1,1,1);

function resizeImage($imagePath, $width, $height, $filterType, $blur, $bestFit, $cropZoom) {
    //The blur factor where &gt; 1 is blurry, &lt; 1 is sharp.
    $imagick = new \Imagick(realpath($imagePath));

    $imagick->resizeImage($width, $height, $filterType, $blur, $bestFit);

    $cropWidth = $imagick->getImageWidth();
    $cropHeight = $imagick->getImageHeight();

    if ($cropZoom) {
        $newWidth = $cropWidth / 2;
        $newHeight = $cropHeight / 2;

        $imagick->cropimage(
            $newWidth,
            $newHeight,
            ($cropWidth - $newWidth) / 2,
            ($cropHeight - $newHeight) / 2
        );

        $imagick->scaleimage(
            $imagick->getImageWidth() * 4,
            $imagick->getImageHeight() * 4
        );
    }


    header("Content-Type: image/jpg");
    echo $imagick->getImageBlob();
}

?>
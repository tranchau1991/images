<?php

//$file='code/source/a.png';
$folder = 'code/source/';
$file= $folder.rand(1, 10).".jpg";
//echo getcwd() ; ///Users/CHAU/Documents/htdoc/test/router

//https://github.com/eventviva/php-image-resize
use \Eventviva\ImageResize;

if(file_exists($file)){
    //echo $file;
}

$image = new ImageResize($file);
//$image->scale(50);
//$image->resize(200, 200);
$image->crop(200, 200);
//$image->save('image2.jpg');
echo $image->output();

die;


//resizeImage($file,'200','200',1,1,1,1);

function resizeImage($imagePath, $width, $height, $filterType, $blur, $bestFit, $cropZoom) {
    //The blur factor where &gt; 1 is blurry, &lt; 1 is sharp.
    $imagick = new \Imagick(realpath($imagePath));

    $imagick->resizeImage($width, $height, $filterType, $blur, $bestFit);

    $cropWidth = $imagick->getImageWidth();
    $cropHeight = $imagick->getImageHeight();

    if ($cropZoom) {
        $newWidth = $cropWidth / 2;
        $newHeight = $cropHeight / 2;

        $imagick->cropimage(
            $newWidth,
            $newHeight,
            ($cropWidth - $newWidth) / 2,
            ($cropHeight - $newHeight) / 2
        );

        $imagick->scaleimage(
            $imagick->getImageWidth() * 4,
            $imagick->getImageHeight() * 4
        );
    }


    header("Content-Type: image/jpg");
    echo $imagick->getImageBlob();
}

?>